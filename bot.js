const TelegramBot = require('node-telegram-bot-api');
const token = require('./token');


const bot = new TelegramBot(token['token'], {polling: true});

bot.onText(/\/help/, (msg) => {
    const chatId = msg.chat.id;
    var resp = "Options: \n /help \n /whois [whatever]"
    bot.sendMessage(chatId, resp);
});

bot.onText(/\/whois (.+)/, (msg, match) => {
    const chatId = msg.chat.id;
    var resp = "I don't know :/";

    if (match[1].toLowerCase() == "d4n1") {
	    resp = match[1] + " is a developer. See d4n1.org";
    }

    bot.sendMessage(chatId, resp);
});

bot.on('message', (msg) => {
    const chatId = msg.chat.id;

    bot.sendMessage(chatId, "It's works :)");
});

