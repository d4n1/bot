# Bot

Telegram bot.

## Requirements

- NodeJS >= 12.21.0
- Telegram :)

## Telegram

```
@BotFather /newbot
```

## Install

```
npm install
```

## vim token.js

```
const = '<token>';
module.exports = { token };
```

## Run

```
node bot.js
```

Access your Telegram bot and start with `/start`.
Add your Telegram bot in some group and test it :)

